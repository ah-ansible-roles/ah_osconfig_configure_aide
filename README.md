# ah_osconfig_configure_aide

An Ansible role for configuring AIDE with a minimal sensible ruleset.

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| Ubuntu | Y | WORKING |

## Changelog

| **Date** | **Description** |
|---|---|
| 2022-06-03 | Initialise AIDE db one doesn't already exist. |
| 2022-04-29 | Initial release. |
